package com.example.zuulgateway;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.EnableZuulServer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.net.URL;

@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
public class ZuulGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulGatewayApplication.class, args);
	}


	@Bean
	public ServletWebServerFactory servletContainer() {
		TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
		tomcat.addAdditionalTomcatConnectors(createSslConnector());

		return tomcat;
	}

	private Connector createSslConnector() {
		Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
		Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();

		connector.setScheme("https");
		connector.setSecure(true);
		connector.setPort(8443);

		protocol.setSSLEnabled(true);
		protocol.setKeystoreType("pkcs12");
		protocol.setKeystorePass("password");
		protocol.setKeyAlias("bose");

		try {
			ClassPathResource keystoreResource = new ClassPathResource("bose.p12");
			URL keystoreUrl = keystoreResource.getURL();
			String keystoreLocation = keystoreUrl.toString();
			protocol.setKeystoreFile(keystoreLocation);
		}
		catch (IOException ex) {
			throw new IllegalStateException("can't access keystore: [keystore]", ex);
		}

		return connector;
	}

}
